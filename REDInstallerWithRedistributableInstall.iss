#define MyAppName "RED"
#define MyAppVersion "1.33a"
#define MyAppPublisher "Merxius"
#define MyAppURL "http://www.merxius.in/"
#define RedEditor "RED Editor"
#define RedViewer "RED Viewer"

#define use_kb835732

#define use_msi20
#define use_msi31
#define use_msi45

#define use_ie6

#define use_dotnetfx11
#define use_dotnetfx11lp

#define use_dotnetfx20
#define use_dotnetfx20lp

#define use_dotnetfx35
#define use_dotnetfx35lp

#define use_dotnetfx40
#define use_wic

#define use_dotnetfx45
#define use_dotnetfx46
#define use_dotnetfx47

#define use_msiproduct
;#define use_vc2005
;#define use_vc2008
#define use_vc2010
#define use_vc2012
#define use_vc2013
#define use_vc2015
;#define use_vc2017

;requires dxwebsetup.exe in src dir
;#define use_directxruntime

#define use_mdac28
#define use_jet4sp8

#define use_sqlcompact35sp2

#define use_sql2005express
#define use_sql2008express

;#define MyAppSetupName 'MyProgram'
;#define MyAppVersion '6.0'

[Setup]

MinVersion=10.0
; Right Side Icon for all wizard pages
WizardSmallImageFile="E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Icons\merxius_icon.bmp"
;WizardImageFile="E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Icons\merxius_icon.bmp"

;To show the Welcome page
DisableWelcomePage=no

;No start menu
DisableProgramGroupPage=yes

;SignTool=MerxiusRedSignTool /d $qRED$q /du $qhttp://merxius.in$q $f
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{88C0CD4B-9FE2-403B-8CEC-00120BE51FE7}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf64}\Merxius\RED Alpha5
DefaultGroupName={#MyAppName}
OutputDir=E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Setup
OutputBaseFilename=RED_v1.33a

;Setup Icon
SetupIconFile=E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\icon_512_new.ico

;Uninstall icon to show in Control Panel
UninstallDisplayIcon=E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\icon_512_new.ico

;License file location
LicenseFile=E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Merxius-EULA for RED-02032018v2.rtf

;Optimising the loading time of installer
SolidCompression=yes
Compression=lzma2/ultra64
LZMAUseSeparateProcess=yes
LZMADictionarySize=1048576
LZMANumFastBytes=273

; supported languages
#include "Dependencies\scripts\lang\english.iss"
;#include "Dependencies\scripts\lang\german.iss"
;#include "Dependencies\scripts\lang\french.iss"
;#include "Dependencies\scripts\lang\italian.iss"
;#include "Dependencies\scripts\lang\dutch.iss"

#ifdef UNICODE
;#include "Dependencies\scripts\lang\polish.iss"
;#include "Dependencies\scripts\lang\russian.iss"
;#include "Dependencies\scripts\lang\japanese.iss"
#endif

[Types]
Name: "custom"; Description: "Custom installation"; Flags: iscustom

[Icons]
Name: "{userdesktop}\{#RedEditor}"; Filename: "{app}\RED_Editor.exe"; Tasks: desktopiconeditor
Name: "{userdesktop}\{#RedViewer}"; Filename: "{app}\RED_Viewer.exe"; Tasks: desktopiconviewer

[Components]

Name: "red_editor"; Description: "RED Editor"; Types: custom
Name: "red_viewer"; Description: "RED Viewer"; Types: custom
Name: "manual_editor"; Description: "RED Editor Manual"; Types: custom
Name: "manual_viewer"; Description: "RED Viewer Manual"; Types: custom
;Name: "red_server"; Description: "RED Server"; Types: custom

[Tasks]
Name: "desktopiconeditor"; Description: "RED Editor"; GroupDescription: "Desktop shortcuts"; Flags: checkablealone; Components: red_editor
Name: "desktopiconviewer"; Description: "RED Viewer"; GroupDescription: "Desktop shortcuts"; Flags: checkablealone; Components: red_viewer

[Files]
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\InnoCallback.dll"; DestDir: "{tmp}"; Flags: dontcopy nocompression
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_Editor.exe"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist; Components: red_editor
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_Editor_Manual.pdf"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist; Components: manual_editor
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_Viewer.exe"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist; Components: red_viewer
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_Viewer_Manual.pdf"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist; Components: manual_viewer
;Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_Server.exe"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist; Components: red_server

Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.AppBuilder.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.Converters.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.Core.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.LevelEditor.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.MaterialEditor.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.OpenGL.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.ProjectCreator.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Editor.UIEditor.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Graphics.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Graphics.OpenGL.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Input.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Networking.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Physics2D.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Platforms.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Platforms.Windows.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Rendering2D.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Rendering3D.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Rendering3D.Shapes.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\DeltaEngine.Scenes.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\EmptyLibrary.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\GalaSoft.MvvmLight.Extras.WPF4.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\GalaSoft.MvvmLight.WPF4.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\Ionic.Zip.Reduced.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\Jitter.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\NAudio.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\OpenAL32.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Junk files\System.Windows.Interactivity.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist

;Dependencies
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\bin\MyProgramDependencies\readme.txt"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\bin\MyProgram-6.0.exe"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\isxdl\isxdl.dll"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\isxdl\isxdl.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\isxdl\english.ini"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\lang\english.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\stringversion.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\winversion.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\fileversion.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\dotnetfxversion.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\msiproduct.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\vcredist2013.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\vcredist2010.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist
Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Dependencies\scripts\products\vcredist2015.iss"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist

; Get License Path
Source: {code:GetLicensePath}; DestDir: "{commonappdata}\RED"; Flags: external
; Adding sample files to user specified location
;Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Sample files\sometext.txt"; DestDir: {code:CopyDir}      

; NOTE: Don't use "Flags: ignoreversion" on any shared system files

Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\RED_License_Cleaner.exe"; DestDir: "{app}"; Flags: ignoreversion onlyifdoesntexist

; Add the ISSkin DLL used for skinning Inno Setup installations.
;Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Skins\ISSkin.dll"; DestDir: {app}; Flags: dontcopy

; Add the Visual Style resource contains resources used for skinning,
; you can also use Microsoft Visual Styles (*.msstyles) resources.
;Source: "E:\Vinod\WorkDocs_Merxius_Sync\Shared With Me\RED\Installer Builds\Sources\Skins\Office2007.cjstyles"; DestDir: {tmp}; Flags: dontcopy

[UninstallRun]
Filename: "{app}\RED_License_Cleaner.exe"

[CustomMessages]
DependenciesDir=MyProgramDependencies
WindowsServicePack=Windows %1 Service Pack %2

; shared code for installing the products
#include "Dependencies\scripts\products.iss"

; helper functions
#include "Dependencies\scripts\products\stringversion.iss"
#include "Dependencies\scripts\products\winversion.iss"
#include "Dependencies\scripts\products\fileversion.iss"
#include "Dependencies\scripts\products\dotnetfxversion.iss"

; actual products

#ifdef use_msiproduct
#include "Dependencies\scripts\products\msiproduct.iss"
#endif

#ifdef use_vc2013
#include "Dependencies\scripts\products\vcredist2013.iss"
#endif
#ifdef use_vc2010
#include "Dependencies\scripts\products\vcredist2010.iss"
#endif
#ifdef use_vc2015
#include "Dependencies\scripts\products\vcredist2015.iss"
#endif

[Code]

// Importing LoadSkin API from ISSkin.DLL
//procedure LoadSkin(lpszPath: String; lpszIniFileName: String);
//external 'LoadSkin@files:isskin.dll stdcall';

// Importing UnloadSkin API from ISSkin.DLL
//procedure UnloadSkin();
//external 'UnloadSkin@files:isskin.dll stdcall';

// Importing ShowWindow Windows API from User32.DLL
//function ShowWindow(hWnd: Integer; uType: Integer): Integer;
//external 'ShowWindow@user32.dll stdcall';

//procedure DeinitializeSetup();
//begin
  // Hide Window before unloading skin so user does not get
  // a glimpse of an unskinned window before it is closed.
 // ShowWindow(StrToInt(ExpandConstant('{wizardhwnd}')), 0);
 // UnloadSkin();
//end;




function InitializeSetup(): boolean;
begin
//ExtractTemporaryFile('Office2007.cjstyles');
 // LoadSkin(ExpandConstant('{tmp}\Office2007.cjstyles'), 'NormalSilver.ini');
  Result := True;

if not IsWin64 then
  begin                     
    SuppressibleMsgBox('Error:The Windows version is 32bit', mbError, MB_OK, MB_OK);
    Result := False;
  end;
	// initialize windows version
	initwinversion();

#ifdef use_vc2013
	//SetForceX86(true); // force 32-bit install of next products
vcredist2013('12'); // min allowed version is 12.0
	//SetForceX86(false); // disable forced 32-bit install again
#endif
#ifdef use_vc2010
	//SetForceX86(true); // force 32-bit install of next products
vcredist2010('10'); // min allowed version is 12.0
	//SetForceX86(false); // disable forced 32-bit install again
#endif
#ifdef use_vc2015
	//SetForceX86(true); // force 32-bit install of next products
vcredist2015('14.0'); // min allowed version is 12.0
	//SetForceX86(false); // disable forced 32-bit install again
#endif

	Result := true;
end;

// For implementing component installation of editor and viewer along with their manuals
var
TimerID: Integer;

type
TTimerProc = procedure(Wnd: HWND; Msg: UINT; TimerID: UINT_PTR; 
SysTime: DWORD);
function WrapTimerProc(Callback: TTimerProc; ParamCount: Integer): LongWord;
external 'wrapcallback@files:InnoCallback.dll stdcall';    
function SetTimer(hWnd: HWND; nIDEvent, uElapse: UINT;
lpTimerFunc: UINT): UINT; external 'SetTimer@user32.dll stdcall';

function KillTimer(hWnd: HWND; uIDEvent: UINT): BOOL; 
external 'KillTimer@user32.dll stdcall'; 

procedure KillComponentsTimer;
begin
  if TimerID <> 0 then 
    begin
      if KillTimer(0, TimerID) then
        TimerID := 1;
    end;
end;

  

var
count: Integer;
seditor: String;
sviewer: String;
 
procedure OnComponentsCheck(Wnd: HWND; Msg: UINT; TimerID: UINT_PTR; 
SysTime: DWORD);
begin
Count := 0;
    if IsComponentSelected('red_editor') then begin
Count := Count + 1;
        WizardForm.ComponentsList.Checked[2] := True;
        WizardForm.ComponentsList.ItemEnabled[2] := True;
        seditor := 'RED Editor';
    end
    else begin
        WizardForm.ComponentsList.Checked[2] := False;
        WizardForm.ComponentsList.ItemEnabled[2] := False;
        seditor := '';
    end; 
    
    if IsComponentSelected('red_viewer') then begin
    Count := Count + 1;
        WizardForm.ComponentsList.Checked[3] := True;
        WizardForm.ComponentsList.ItemEnabled[3] := True;
        sviewer := 'RED Viewer';

    end
    else begin
        WizardForm.ComponentsList.Checked[3] := False;
        WizardForm.ComponentsList.ItemEnabled[3] := False;
        sviewer := '';

    end;
    
    if Count < 1 then
    begin
        WizardForm.NextButton.Enabled := False;
    end
    else begin
        WizardForm.NextButton.Enabled := True;
        end;


end;

procedure ComponentsCheck();
var
  TimerCallback: LongWord;
begin
  TimerCallback := WrapTimerProc(@OnComponentsCheck, 4);
  TimerID := SetTimer(0, 0, 100, TimerCallback);
end;            

procedure CurPageChanged(CurPageID: Integer);
begin
if CurPageID = wpSelectComponents then
  begin
    ComponentsCheck;
  end;
if CurPageID = not wpSelectComponents then 
  begin
   KillComponentsTimer;
  end;

  if CurPageID = wpReady then
  begin
    Wizardform.ReadyMemo.Lines.Add(' '); { Empty string }
    Wizardform.ReadyMemo.Lines.Add(' '); { Empty string }
    Wizardform.ReadyMemo.Lines.Add('Selected components : ' + seditor + '   ' + sviewer);
  end;
end;



//For getting License file and saving to pdp

  procedure FilePageEditChange(Sender: TObject);                                             // To enable and disable
  begin                                                                                    //
    WizardForm.NextButton.Enabled := (Length(TEdit(Sender).Text) > 0);                     // Next button if no 
  end;                                                                                     //
                                                                                           //
procedure FilePageActivate(Sender: TWizardPage);                                           // License file is selected
  begin                                                                                    //
    FilePageEditChange(TInputFileWizardPage(Sender).Edits[0]);                             //
  end;                                                                                    //

// { Open a window to browse License file}  

var
  Edit: TEdit;
var
  LicenseFilePage: TInputFileWizardPage;
  CopyDirPage: TInputDirWizardPage;

var
  CompsToInstallPage: TOutputMsgMemoWizardPage;

procedure InitializeWizard();
 begin
 if not FileExists(ExpandConstant('{commonappdata}\RED')) then
 begin
  LicenseFilePage :=
    CreateInputFilePage( wpSelectDir, 'Select License File Location', 'Where is your license file located?',
    'Select where License file is located, then click Next.');
    LicenseFilePage.OnActivate := @FilePageActivate;                                       // To update the Next button state when the page is entered
    //Edit := LicenseFilePage.Edits[LicenseFilePage.Add('Location of license file:', 'All files|*.*', '.lic')];
    Edit := LicenseFilePage.Edits[LicenseFilePage.Add('Location of license file:', 'RED License|*.r3dlicense*', '.lic')];
    Edit.OnChange := @FilePageEditChange;                                                  // To update the Next button state when the edit contents changes       
 
    // Window to select location where to save sample files
    //CopyDirPage := CreateInputDirPage(wpSelectDir, 'Select folder where to save the sample files', '',  '', False, '');
    //CopyDirPage.Add('Source folder:');
 end
 //else
   // MsgBox('License file found on the device. Press OK to continue', mbInformation, MB_OK); 
end;
 
 // For getting the path where license file is.
function GetLicensePath(Param: string): string;
begin
  if Assigned(LicenseFilePage) then
  begin
   Result := LicenseFilePage.Values[0]
   //RenameFile(LicenseFilePage.Values[0], ExpandConstant('{app}\REDLicense.zip'));
  end
  else 
   Result := '';
end;




// Unzipping the license file user selects
const                                                                                           //
  SHCONTCH_NOPROGRESSBOX = 4;                                                                   //
  SHCONTCH_RESPONDYESTOALL = 16;                                                                //
                                                                                                //
procedure Unzip(ZipFile, TargetFolder: String);                                                 //
var                                                                                             //
  ShellObj, SrcFile, DestFolder: Variant;                                                       //
begin                                                                                           //
  ShellObj := CreateOleObject('Shell.Application');                                             //
  SrcFile := ShellObj.NameSpace(ZipFile);                                                       //
  DestFolder := ShellObj.NameSpace(TargetFolder);                                               //
  DestFolder.CopyHere(SrcFile.Items, SHCONTCH_NOPROGRESSBOX or SHCONTCH_RESPONDYESTOALL);        //
end;                                                                                            //
                                                                                                //
procedure ExtractMe(src, target : String);                                                      //
begin                                                                                           //
  // Add extra application code here, then:
  //RenameFile(ExpandConstant('{app}\' + src), ExpandConstant('{app}\' + 'REDLicense.zip'));                                                     //
  Unzip(ExpandConstant(src), ExpandConstant(target));                                           //
end;                                                                                            //


// For getting path to save sample files
//function CopyDir(Params: string): string;
//begin
 // if Assigned(CopyDirPage) then
 // Result := CopyDirPage.Values[0]
  // else 
  // Result := '';
//end;


[Registry]
Root: HKCU; Subkey: "Software\Merxius\RED"; Flags: uninsdeletekey; ValueType: string; ValueName: "Path"; ValueData: "{app}"

Root: HKCR; Subkey: "Software\Merxius\RED\.r3d"; ValueType: string; ValueName: ""; ValueData: "{app}\RED_Viewer.EXE"; Flags: uninsdeletevalue
Root: HKCU; Subkey: "Software\Merxius\RED"; ValueType: string; ValueName: "ViewerHelpDir"; ValueData: "{app}"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\Merxius\RED"; ValueType: string; ValueName: "DefaultIconViewer"; ValueData: "{app}\RED_Viewer.EXE,0"
Root: HKCR; Subkey: "Software\Merxius\RED\ViewerInstallDir\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\RED_Viewer.EXE"" open ""%1"""

Root: HKCR; Subkey: "Software\Merxius\RED\.r3dproject"; ValueType: string; ValueName: ""; ValueData: "{app}\RED_Editor.EXE"; Flags: uninsdeletevalue
Root: HKCU; Subkey: "Software\Merxius\RED"; ValueType: string; ValueName: "EditorHelpDir"; ValueData: "{app}"; Flags: uninsdeletekey
Root: HKCU; Subkey: "Software\Merxius\RED"; ValueType: string; ValueName: "DefaultIconEditor"; ValueData: "{app}\RED_Editor.EXE,0"
Root: HKCR; Subkey: "Software\Merxius\RED\EditorInstallDir\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\RED_Editor.EXE"" open ""%1"""

Root: "HKCU"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"; ValueType: String; ValueName: "{app}\RED_Viewer.EXE";ValueData: "RUNASADMIN";  Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion:10.0
Root: "HKCU"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"; ValueType: String; ValueName: "{app}\RED_Editor.EXE";ValueData: "RUNASADMIN";  Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion:10.0
Root: "HKCU"; Subkey: "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\"; ValueType: String; ValueName: "{app}\RED_License_Cleaner.EXE";ValueData: "RUNASADMIN";  Flags: uninsdeletekeyifempty uninsdeletevalue; MinVersion:10.0



