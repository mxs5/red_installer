;contribute: http://github.com/stfx/innodependencyinstaller
;original article: http://codeproject.com/Articles/20868/NET-Framework-1-1-2-0-3-5-Installer-for-InnoSetup

;comment out product defines to disable installing them
;#define use_iis
#define use_kb835732

#define use_msi20
#define use_msi31
#define use_msi45

#define use_ie6

#define use_dotnetfx11
#define use_dotnetfx11lp

#define use_dotnetfx20
#define use_dotnetfx20lp

#define use_dotnetfx35
#define use_dotnetfx35lp

#define use_dotnetfx40
#define use_wic

#define use_dotnetfx45
#define use_dotnetfx46
#define use_dotnetfx47

#define use_msiproduct
#define use_vc2005
#define use_vc2008
#define use_vc2010
#define use_vc2012
#define use_vc2013
#define use_vc2015
#define use_vc2017

;requires dxwebsetup.exe in src dir
;#define use_directxruntime

#define use_mdac28
#define use_jet4sp8

#define use_sqlcompact35sp2

#define use_sql2005express
#define use_sql2008express

#define MyAppSetupName 'MyProgram'
#define MyAppVersion '6.0'

[Setup]
AppName={#MyAppSetupName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppSetupName} {#MyAppVersion}
AppCopyright=Copyright © 2007-2017 stfx
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany=stfx
AppPublisher=stfx
;AppPublisherURL=http://...
;AppSupportURL=http://...
;AppUpdatesURL=http://...
OutputBaseFilename={#MyAppSetupName}-{#MyAppVersion}
DefaultGroupName={#MyAppSetupName}
DefaultDirName={pf}\{#MyAppSetupName}
UninstallDisplayIcon={app}\MyProgram.exe
OutputDir=bin
SourceDir=.
AllowNoIcons=yes
;SetupIconFile=MyProgramIcon
SolidCompression=yes

;MinVersion default value: "0,5.0 (Windows 2000+) if Unicode Inno Setup, else 4.0,4.0 (Windows 95+)"
;MinVersion=0,5.0
PrivilegesRequired=admin
ArchitecturesAllowed=x86 x64 ia64
ArchitecturesInstallIn64BitMode=x64 ia64

; downloading and installing dependencies will only work if the memo/ready page is enabled (default and current behaviour)
DisableReadyPage=no
DisableReadyMemo=no

; supported languages
#include "scripts\lang\english.iss"
#include "scripts\lang\german.iss"
#include "scripts\lang\french.iss"
#include "scripts\lang\italian.iss"
#include "scripts\lang\dutch.iss"

#ifdef UNICODE
#include "scripts\lang\polish.iss"
#include "scripts\lang\russian.iss"
#include "scripts\lang\japanese.iss"
#endif

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "src\MyProgram-x64.exe"; DestDir: "{app}"; DestName: "MyProgram.exe"; Check: IsX64
Source: "src\MyProgram-IA64.exe"; DestDir: "{app}"; DestName: "MyProgram.exe"; Check: IsIA64
Source: "src\MyProgram.exe"; DestDir: "{app}"; Check: not Is64BitInstallMode




[Icons]
Name: "{group}\{#MyAppSetupName}"; Filename: "{app}\MyProgram.exe"
Name: "{group}\{cm:UninstallProgram,{#MyAppSetupName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppSetupName}"; Filename: "{app}\MyProgram.exe"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppSetupName}"; Filename: "{app}\MyProgram.exe"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\MyProgram.exe"; Description: "{cm:LaunchProgram,{#MyAppSetupName}}"; Flags: nowait postinstall skipifsilent

[CustomMessages]
DependenciesDir=MyProgramDependencies
WindowsServicePack=Windows %1 Service Pack %2

; shared code for installing the products
#include "scripts\products.iss"

; helper functions
#include "scripts\products\stringversion.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfxversion.iss"

; actual products


#ifdef use_msiproduct
#include "scripts\products\msiproduct.iss"
#endif

#ifdef use_vc2010
#include "scripts\products\vcredist2010.iss"
#endif

#ifdef use_vc2015
#include "scripts\products\vcredist2015.iss"
#endif


[Code]
function InitializeSetup(): boolean;
begin
	// initialize windows version
	initwinversion();


#ifdef use_vc2010
	//SetForceX86(true); // force 32-bit install of next products
	vcredist2010('10'); // min allowed version is 12.0
	//SetForceX86(false); // disable forced 32-bit install again
#endif

#ifdef use_vc2015
	//SetForceX86(true); // force 32-bit install of next products
	vcredist2015('14'); // min allowed version is 12.0
	//SetForceX86(false); // disable forced 32-bit install again
#endif


	Result := true;
end;
